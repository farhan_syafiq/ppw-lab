// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  }
   else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if (x === 0) {
    if(print.value != "") {
      print.value += x;
    }
  }
  else if(x === 'log'){
    print.value = Math.log10(print.value);
  }
  else if(x === 'sin'){
    print.value = Math.sin(print.value * Math.PI / 180);
  }
  else if(x === 'tan'){
    print.value = Math.tan(print.value * Math.PI / 180);
  }
   else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//select2

var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var defaultTheme = {"id":3,"text":"Indigo", "bcgColor":"#3F51B5","fontColor":"#FAFAFA"};

$(document).ready(function() {
  if(!localStorage.themes) {
    localStorage.themes = JSON.stringify(themes);
    localStorage.selectedTheme = JSON.stringify(defaultTheme);
  }

var sender = true;
$("textarea").keypress(function(e){
  if(e.keyCode === 13 && !e.shiftKey){
    var c = String.fromCharCode(e.which);
    var textValue = $('textarea').val();
    var fulltext = textValue + c;
    $("textarea").val(" ");
    if(sender){
      $(".msg-insert").append('<p class="msg-send">' + fulltext + '</p>')
      sender=false;
    }
    else{
      $(".msg-insert").append('<p class="msg-receive">' + fulltext + '</p>')
      sender=true;
    }
    

    e.preventDefault();
  }
});

  $('.my-select').select2({
  'data' : JSON.parse(localStorage.themes)
  });

  $('.apply-button').on('click', function(){ 
      var data = JSON.parse(localStorage.themes);
      var id = $(".my-select").select2("val");
      $.each(data, function(i, theme) {
        if(id == theme.id) {
          $("body").css({"background-color" : theme.bcgColor, "color" : theme.fontColor});
          var selected = {
            "id":theme.id,
            "bcgColor":theme.bcgColor,
            "fontColor" : theme.fontColor
          };
          localStorage.selectedTheme = JSON.stringify(selected);
          return false; 
      }
    });
  });
});